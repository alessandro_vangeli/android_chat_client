package com.example.alessandrovangeli.chat_client;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by alessandro.vangeli on 29/11/2017.
 */

public class MyChatAdapter extends RecyclerView.Adapter<MyChatAdapter.ViewHolder> {

    private final Context context;
    private List<Message> messages;

    public MyChatAdapter(Context context, List<Message> messages){
        this.messages =  messages;
        this.context = context;
    }


    @Override
    public MyChatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(context).inflate(R.layout.user_message, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyChatAdapter.ViewHolder viewHolder, int position) {
        Message messaggio = messages.get(position);
        viewHolder.mUsernameView.setText(messaggio.getMittente());
        viewHolder.mMessageView.setText(messaggio.getMessaggio());

        String tipo = messaggio.getTipo();
        if(tipo.equals("verde")){
            int color = Color.parseColor("#c0ead7");
            viewHolder.mCardView.setCardBackgroundColor(color);
        }else{
            int color = Color.parseColor("#fff0f0");
            viewHolder.mCardView.setCardBackgroundColor(color);
        }

    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public CardView mCardView;
        public TextView mUsernameView;
        public TextView mMessageView;

        public ViewHolder(View itemView) {
            super(itemView);

            mCardView = itemView.findViewById(R.id.card_view);
            mUsernameView = itemView.findViewById(R.id.username);
            mMessageView = itemView.findViewById(R.id.message);
        }

    }
}
