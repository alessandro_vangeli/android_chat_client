package com.example.alessandrovangeli.chat_client.RetrofitService;

import java.util.ArrayList;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by alessandro.vangeli on 16/01/2018.
 */

public interface RetrofitServiceInterface {

    @GET("/firebase_registration_prova")
    Call<String> registerUserProva(@Query("username") String user, @Query("token") String token);

    @GET("/firebase_registration")
    Call<String> registerUser(@QueryMap Map<String,String> f );

}
