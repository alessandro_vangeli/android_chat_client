package com.example.alessandrovangeli.chat_client.RetrofitService;

/**
 * Created by alessandro.vangeli on 16/01/2018.
 */

public class ApiUtils {


    public static final String BASE_URL = "http://172.31.98.217:3000";

    public static RetrofitServiceInterface getUserMainervice() {
        return RetrofitClient.getClient(BASE_URL).create(RetrofitServiceInterface.class);
    }
}
