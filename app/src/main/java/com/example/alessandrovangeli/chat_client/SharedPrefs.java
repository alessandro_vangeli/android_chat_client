package com.example.alessandrovangeli.chat_client;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by alessandro.vangeli on 18/01/2018.
 */

public class SharedPrefs {

    private static final String PREF_FILE_NAME = "ChatPref";
    private static final String FIREBASE_TOKEN = "Firebase_token";
    private static final String LOG_STATE = "log_state";
    public static final String CHAT_ACTIVITY_STATE = "chat_activity_state";


    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
    }


    public static boolean contains(Context context, String key){
        SharedPreferences prefs = getSharedPreferences(context);

        return prefs.contains(key);
    }

    public static void saveFirebaseToken(Context context, String token) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(FIREBASE_TOKEN, token);
        editor.commit();

    }


    public static String getFirebaseToken(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        String token = prefs.getString(FIREBASE_TOKEN, null);
        return token;
    }



    public static void saveUser(Context context, String utente) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(LOG_STATE, "logged_in");
        editor.putString("user_logged", utente);
        editor.commit();

    }

    public static void removeUser(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(LOG_STATE, "logged_out");
        editor.remove("user_logged");
        editor.commit();

    }

    public static String getLogState(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        String flag = prefs.getString(LOG_STATE, null);
        return flag;
    }

    public static String getUser(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        String utente = prefs.getString("user_logged", null);
        return utente;
    }

    public static void saveChatActivityState(Context context, String stato){
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(CHAT_ACTIVITY_STATE, stato);
        editor.commit();

    }

    public static String getChatActivityState(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        String stato = prefs.getString(CHAT_ACTIVITY_STATE, null);
        return stato;
    }

}
