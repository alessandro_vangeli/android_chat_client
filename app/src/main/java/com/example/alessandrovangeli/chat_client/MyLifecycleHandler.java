package com.example.alessandrovangeli.chat_client;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by alessandro.vangeli on 01/02/2018.
 */

public class MyLifecycleHandler implements android.app.Application.ActivityLifecycleCallbacks {

    private static int resumed;
    private static int paused;
    private static int started;
    private static int stopped;

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

        ++started;

    }

    @Override
    public void onActivityResumed(Activity activity) {

        ++resumed;
    }

    @Override
    public void onActivityPaused(Activity activity) {

        ++paused;
    }

    @Override
    public void onActivityStopped(Activity activity) {

        ++stopped;
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }

    public static boolean isApplicationVisible() {

        boolean bool = started > stopped;
        return bool;
    }

    public static boolean isApplicationInForeground() {

        boolean bool = resumed > paused;
        return bool ;
    }

    public static boolean isComingFromBackGround(){

        boolean bool = stopped <= paused;
        return bool ;
    }

}
