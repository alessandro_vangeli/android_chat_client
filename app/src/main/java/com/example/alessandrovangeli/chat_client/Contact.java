package com.example.alessandrovangeli.chat_client;

/**
 * Created by alessandro.vangeli on 08/02/2018.
 */

public class Contact {

    private String username;
    private boolean log_flag, message_received;

    public Contact(String username, boolean log_flag, boolean message_received ){

        this.log_flag = log_flag; this.username = username; this.message_received = message_received;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Boolean getLogFlag() {
        return log_flag;
    }

    public void setFlag(Boolean log_flag) {
        this.log_flag = log_flag;
    }

    public void setMessageReceived(Boolean message_received) {
        this.message_received = message_received;
    }

    public Boolean getMessageReceived() {
        return message_received;
    }


    public String toString(){
        return "user: "+username+", status: "+log_flag+ ", notifica"+message_received;
    }

    public boolean equals(Object obj){

        boolean bool = false;
        if(obj instanceof Contact){
            Contact contact = (Contact) obj;
            if(((contact).getUsername()).equals(this.username) && contact.log_flag == this.getLogFlag()){
                bool = true;
            }
        }

        return bool;
    }
}
