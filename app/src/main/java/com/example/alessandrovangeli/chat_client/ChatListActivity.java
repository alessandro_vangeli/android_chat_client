
package com.example.alessandrovangeli.chat_client;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;

import com.example.alessandrovangeli.chat_client.FirebaseService.MyFirebaseMessagingService;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class ChatListActivity extends AbstractActivity implements AdapterView.OnItemClickListener, MyContactListAdapter.OnMessageNotification{

    private Socket socket;

    //private MyFirebaseMessagingService firebaseMessagingService;
    private RecyclerView recyclerView;
    public MyContactListAdapter myContactListAdapter;
    private ArrayList<Contact> utenti = new ArrayList<Contact>();
    private String mittente;

    private TextView titleView;
    private Button logoutButton;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_list);

        Logger.addLogAdapter(new AndroidLogAdapter());

        MyFirebaseMessagingService.setListener(ChatListActivity.this);
        recyclerView = (RecyclerView) findViewById(R.id.lista_utenti);

        String token = SharedPrefs.getFirebaseToken(getApplicationContext());
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        titleView = (TextView) myToolbar.findViewById(R.id.title);
        logoutButton = myToolbar.findViewById(R.id.logout);

        Bundle extras;
        Intent intent = getIntent();
        if(intent.hasExtra("mittente")){
            extras = intent.getExtras();
            mittente = extras.getString("mittente");
        }else{
            mittente = SharedPrefs.getUser(getApplicationContext());
        }

        titleView.setText(mittente);

        socket = super.socket;
        socket.on(Socket.EVENT_RECONNECT, onConnect);
        socket.on("get users", onGetUsers);
        socket.on("update_list", onUpdateList);
        socket.connect();
        socket.emit("firebase_registration", token);
        socket.emit("get users", mittente);


    }


    @Override
    protected void onStart() {
        super.onStart();


        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPrefs.removeUser(getApplicationContext());
                socket.disconnect();
                socket.emit("logout", mittente);
                Intent in = new Intent(ChatListActivity.this, LoginActivity.class);
                startActivity(in);
                finish();
            }
        });

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        socket.emit("status_offline", mittente);
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (myContactListAdapter != null) {
            myContactListAdapter.notifyDataSetChanged();
        }

    }

    @Override
    protected void onStop() {
        super.onStop();


    }

    private void setRecycler() {
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(manager);
        myContactListAdapter = new MyContactListAdapter( this, utenti, ChatListActivity.this);
        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(mDividerItemDecoration);
        recyclerView.setAdapter(myContactListAdapter);

    }


    private Emitter.Listener onGetUsers = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONArray nomiUtente = (JSONArray) args[0];

                    for(int i=0; i<nomiUtente.length(); i++){
                        JSONObject contatto = null;
                        String nome = null;
                        String status = null;
                        try {
                            contatto = nomiUtente.getJSONObject(i);
                            nome = contatto.getString("username");
                            status = contatto.getString("status");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        boolean message_received = false;
                        if(status.equals("online")){
                            utenti.add(new Contact(nome,true,message_received));
                        }else{
                            utenti.add(new Contact(nome,false,message_received));
                        }

                    }

                    setRecycler();
                }
            });

        }
    };

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    socket.emit("reconnection", mittente);
                }
            });

        }
    };

    private Emitter.Listener onUpdateList = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    JSONObject data = (JSONObject) args[0];
                    String username = null;
                    String azione = null;

                    try {
                        username = data.getString("username");
                        azione = data.getString("azione");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    for (Contact dato : utenti) {
                        Log.d("Data",dato.toString());
                    }

                    Contact contatto ;
                    boolean status = true;
                    boolean message_received = false;

                    if(azione.equals("aggiungi")){
                        //contatto già online
                        contatto = new Contact(username, status, message_received);
                        if(utenti.contains(contatto)) return;

                        //controllo se c'è già il contatto offline in lista
                        status = false;
                        contatto = new Contact(username, status, message_received);
                        Log.d("contatto", contatto.toString());
                        if(!utenti.contains(contatto)){

                            contatto.setFlag(true);
                            utenti.add(contatto);
                        }else{
                            contatto = getContatto(username);
                            contatto.setFlag(true);
                        }

                    }else{
                        contatto = getContatto(username);
                        contatto.setFlag(false);
                    }

                    myContactListAdapter.notifyDataSetChanged();
                }
            });

        }
    };


    private Contact getContatto(String username){

        Contact contatto = null ;
        for(int i = 0; i< utenti.size(); i++){
            contatto = utenti.get(i);
            if((contatto.getUsername()).equals(username)){
                break;
            }
        }
        return contatto;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        Contact contact = myContactListAdapter.getItem(i);
        String destinatario = contact.getUsername();
        contact.setMessageReceived(false);

        Intent in = new Intent(ChatListActivity.this, ChatActivity.class);
        Bundle extras = new Bundle();

        extras.putString("destinatario", destinatario);
        extras.putString("mittente", mittente);
        in.putExtras(extras);

        startActivity(in);

    }


    @Override
    public void onMessageNotification(String user) {

        Contact contact = getContatto(user);
        contact.setMessageReceived(true);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                myContactListAdapter.notifyDataSetChanged();
            }
        });

    }
}
