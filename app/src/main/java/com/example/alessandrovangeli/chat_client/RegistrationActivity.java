package com.example.alessandrovangeli.chat_client;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class RegistrationActivity extends AppCompatActivity {

    private EditText username_text;
    private EditText password_text;
    private Button sign_in_button;
    private Socket socket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        SocketImpl appl = (SocketImpl)getApplication();
        socket = appl.getSocket();

        username_text = (EditText) findViewById(R.id.username_input);
        password_text = (EditText) findViewById(R.id.password_input);

        sign_in_button = (Button) findViewById(R.id.sign_in_button);
        sign_in_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });

        socket.on("status", OnStatus);
        socket.connect();

    }

    private void signIn() {

        String username = username_text.getText().toString().trim();
        String password = password_text.getText().toString().trim();
        Utente utente = new Utente(username, password);
        String json_utente = new Gson().toJson(utente);

        if(username.equals("") || password.equals(""))
            Toast.makeText(RegistrationActivity.this, "Inserire la username e la password" +
                    "!", Toast.LENGTH_LONG).show();

        socket.emit("sign_in", json_utente);

    }

    private Emitter.Listener OnStatus = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String status = null;

                    try {
                        status = data.getString("message");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    switch (status) {

                        case "succesfull registration":
                            Toast.makeText(RegistrationActivity.this, "utente registrato" + "!", Toast.LENGTH_LONG).show();

                            Intent intent = new Intent(RegistrationActivity.this, LoginActivity.class );
                            startActivity(intent);
                            finish();
                            break;
                        case "unable registration":
                            Toast.makeText(RegistrationActivity.this, "utente già registrato" + "!", Toast.LENGTH_LONG).show();


                    }
                }

            });
        }
    };

}
