package com.example.alessandrovangeli.chat_client;

import android.app.DownloadManager;
import android.app.Notification;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class ChatActivity extends AbstractActivity {


    private Socket socket;

    RecyclerView recyclerView;
    ArrayList<Message> messages = new ArrayList<Message>();
    MyChatAdapter myChatAdapter;

    private String mittente;
    private String destinatario;
    private EditText inputMessage;
    private ImageButton invia;
    private TextView textTyping;

    private TextView usersNumber;
    private TextView titleView;
    private ArrayList<String> membri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_chat);
        recyclerView = (RecyclerView) findViewById(R.id.messages);
        inputMessage = (EditText) findViewById(R.id.message_input);
        textTyping = (TextView) findViewById(R.id.user_typing);
        usersNumber = (TextView) findViewById(R.id.users_number);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        myToolbar.setTitle("Chat ");
        titleView = (TextView) myToolbar.findViewById(R.id.title);

        Bundle extras = getIntent().getExtras();
        destinatario = extras.getString("destinatario");
        mittente = extras.getString("mittente");
        titleView.setText(destinatario);

        socket = super.socket;
        socket.on(Socket.EVENT_RECONNECT, onConnect);
        socket.on("new message", onNewMessage);
        socket.on("conversation_update", onConversationUpdate);
        socket.on("typing", onTyping);
        socket.connect();

        membri = new ArrayList<String>();
        membri.add(mittente);
        membri.add(destinatario);
        String json_data = new Gson().toJson(membri);

        socket.emit("conversation_update", json_data);
        //visualizza numero partecipanti
        //logNumUsers(numUsers);

    }

    @Override
    protected void onStart() {
        super.onStart();

        SharedPrefs.saveChatActivityState(getApplicationContext(), "aperta");


        inputMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {



            }

            @Override
            public void afterTextChanged(Editable editable) {

                if(!(editable.toString().equals(""))){
                    Message data = new Message.Builder().setMittente(mittente).setDestinatario(destinatario).build();
                    String json_data = new Gson().toJson(data);
                    socket.emit("typing", json_data);
                }
            }
        });

        invia = (ImageButton) findViewById(R.id.send_button);
        invia.setOnClickListener(new View.OnClickListener(){


            @Override
            public void onClick(View view) {
                sendMessage();
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();

        SharedPrefs.saveChatActivityState(getApplicationContext(), "non aperta");
    }

    private void setRecycler() {

        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(manager);
        myChatAdapter = new MyChatAdapter(this, messages);
        recyclerView.setAdapter(myChatAdapter);
        scrollToBottom();
        //Log.d(TAG,messages.get(messages.size()-1).getMessaggio());
        //Log.d(TAG,messages.get(messages.size()-1).getUsername());
    }



    private void logNumUsers(int numUtenti) {

        if(numUtenti>=2)
            usersNumber.setText("ci sono "+numUtenti+" utenti collegati\n");
        else
            usersNumber.setText("c'è "+numUtenti+" utente collegato\n");
    }

    private void addMessage(String username, String message, String tipo) {
        messages.add(new Message.Builder().setMittente(username).setMessaggio(message).setTipo(tipo).build());
        //Log.d(TAG, messages.get(messages.size()-1).getUsername());
        //Log.d(TAG, messages.get(messages.size()-1).getMessaggio());
        myChatAdapter.notifyDataSetChanged();
        scrollToBottom();
    }

    private void addAction(String username, String action) {
        messages.add(new Message.Builder().setMittente(username).setMessaggio(action).build());
        myChatAdapter.notifyDataSetChanged();
        scrollToBottom();
    }

    private void scrollToBottom() {
        recyclerView.scrollToPosition(myChatAdapter.getItemCount() - 1);
    }

    //

    //invio messaggio
    private void sendMessage() {

        String messaggio = inputMessage.getText().toString();
        Message data = new Message.Builder().setMittente(mittente).setDestinatario(destinatario).setMessaggio(messaggio).build();
        addMessage( "io", " " + messaggio, Message.MINE);

        String json_data = new Gson().toJson(data);

        socket.emit("new message", json_data);
        inputMessage.setText("");

    }

    private Emitter.Listener onConversationUpdate = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONArray messaggi = (JSONArray) args[0];

                    for(int i=0; i<messaggi.length(); i++){
                        JSONObject messaggio_json = null;
                        String username = null;
                        String testo = null;
                        try {
                            messaggio_json = messaggi.getJSONObject(i);
                            username = messaggio_json.getString("username");
                            testo = messaggio_json.getString("testo");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Message messaggio = null;

                        if(username.equals(mittente)){
                            username = "io";
                            messaggio = new Message.Builder().setMittente(username).setMessaggio(testo).setTipo(Message.MINE).build();
                        }else{
                            messaggio = new Message.Builder().setMittente(username).setMessaggio(testo).setTipo(Message.OTHERS).build();
                        }

                        messages.add(messaggio);

                    }


                    setRecycler();
                }
            });

        }
    };


    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable(){

                @Override
                public void run() {

                    JSONObject data = (JSONObject) args[0];
                    String user_mittente = null;
                    String message = null;

                    try {
                        user_mittente = data.getString("user_mittente");
                        message = data.getString("message");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    textTyping.setText("");

                    if(user_mittente.equals(destinatario)){
                        addMessage(user_mittente, " "+message, Message.OTHERS);

                    }


                }

            });

        }
    };

    private Emitter.Listener onTyping = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String user_mittente = (String) args[0];

                    if(user_mittente.equals(destinatario)) {
                        textTyping.setText(" sta scrivendo...");
                    }
                }
            });
        }
    };


    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    socket.emit("reconnection", mittente);
                }
            });

        }
    };

    private Emitter.Listener onUserJoined = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username = new String();
                    int numUsers = 0;
                    try {
                        username = data.getString("username");
                        //numUsers = data.getInt("numUsers");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    addAction(username, " partecipa alla chat" );
                    //logNumUsers(numUsers);
                }
            });
        }
    };

    private Emitter.Listener onUserLeft = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    int numUsers;
                    try {
                        username = data.getString("username");
                        numUsers = data.getInt("numUsers");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return;
                    }

                    addAction(username, " ha lasciato la chat");
                    textTyping.setText("");
                    //logNumUsers(numUsers);

                }
            });

        }
    };
}
