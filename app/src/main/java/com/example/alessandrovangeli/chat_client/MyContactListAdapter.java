package com.example.alessandrovangeli.chat_client;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by alessandro.vangeli on 22/12/2017.
 */

public class MyContactListAdapter extends RecyclerView.Adapter<MyContactListAdapter.ViewHolder>{

    private final Context context;
    private List<Contact> contatti;
    AdapterView.OnItemClickListener onItemClickListener;

    public MyContactListAdapter(Context context, List<Contact> contatti, AdapterView.OnItemClickListener onItemClickListener) {
        this.context = context;
        this.contatti = contatti;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public MyContactListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.username_row, parent, false);
        return new MyContactListAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyContactListAdapter.ViewHolder viewHolder, final int position) {
        String username = contatti.get(position).getUsername();
        boolean flagStatus = contatti.get(position).getLogFlag();
        boolean flagNotification = contatti.get(position).getMessageReceived();
        viewHolder.mUsernameView.setText(username);
        if(flagStatus){
            viewHolder.iconStatus.setVisibility(View.VISIBLE);
        }else{
            viewHolder.iconStatus.setVisibility(View.INVISIBLE);
        }

        if(flagNotification){
            viewHolder.iconMessage.setVisibility(View.VISIBLE);
        }else{
            viewHolder.iconMessage.setVisibility(View.INVISIBLE);
        }

        viewHolder.loMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickListener.onItemClick(null, view, position, view.getId());
            }
        });

    }

    @Override
    public int getItemCount() {
        return contatti.size();
    }

    public Contact getItem(int position) {
        return contatti.get(position);
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        public ConstraintLayout loMain;
        public TextView mUsernameView;
        public ImageView iconStatus, iconMessage;

        public ViewHolder(View itemView) {
            super(itemView);
            loMain = itemView.findViewById(R.id.loMain);
            mUsernameView = itemView.findViewById(R.id.username);
            iconStatus = itemView.findViewById(R.id.icon_status);
            iconMessage = itemView.findViewById(R.id.icon_notification);
        }

    }



    public interface OnMessageNotification{

         void onMessageNotification(String user);

    }
}
