package com.example.alessandrovangeli.chat_client.FirebaseService;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.RequiresApi;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.example.alessandrovangeli.chat_client.ChatActivity;
import com.example.alessandrovangeli.chat_client.ChatListActivity;
import com.example.alessandrovangeli.chat_client.LoginActivity;
import com.example.alessandrovangeli.chat_client.MyContactListAdapter;
import com.example.alessandrovangeli.chat_client.MyLifecycleHandler;
import com.example.alessandrovangeli.chat_client.R;
import com.example.alessandrovangeli.chat_client.SharedPrefs;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private final String TAG = "MyFirebaseMessaging";
    private static MyContactListAdapter.OnMessageNotification listener_;
    String destinatario;
    String mittente;

    public MyFirebaseMessagingService() {
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        String[] notifica = new String[3];

        if(remoteMessage.getData() != null){

            destinatario = remoteMessage.getData().get("destinatario");
            mittente = remoteMessage.getData().get("title");
            String message = remoteMessage.getData().get("body");
            notifica[0] = destinatario;
            notifica[1] = mittente;
            notifica[2] = message;

        }

        String chat_activity_state = SharedPrefs.getChatActivityState(getApplicationContext());
        if(!SharedPrefs.contains(getApplicationContext(), SharedPrefs.CHAT_ACTIVITY_STATE) || chat_activity_state.equals("non aperta")){
            createNotification(notifica);
            listener_.onMessageNotification(mittente);
        }

    }

    public static void setListener(MyContactListAdapter.OnMessageNotification listener){

        listener_ = listener;
    }

    private void createNotification(String[] body) {

        String destinatario = body[0];
        String mittente = body[1];
        String testo = body[2];
        Bundle extras = new Bundle();
        extras.putString("mittente", destinatario);
        extras.putString("destinatario", mittente);


        Intent Chatintent = new Intent( this , ChatActivity.class );
        Chatintent.putExtras(extras);

        Intent ChatListIntent= new Intent(this,ChatListActivity.class);
        ChatListIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

        final PendingIntent pendingIntent = PendingIntent.getActivities(this, 0,
                new Intent[] {ChatListIntent,Chatintent},PendingIntent.FLAG_ONE_SHOT);

        //Chatintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        //PendingIntent resultIntent = PendingIntent.getActivity( this , 0, Chatintent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri notificationSoundURI = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder mNotificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(mittente)
                .setContentText(testo)
                .setAutoCancel( true )
                .setSound(notificationSoundURI)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, mNotificationBuilder.build());
    }


}
