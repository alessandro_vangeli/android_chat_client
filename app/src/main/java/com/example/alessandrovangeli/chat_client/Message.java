package com.example.alessandrovangeli.chat_client;

import android.accessibilityservice.GestureDescription;

/**
 * Created by alessandro.vangeli on 24/11/2017.
 */

public class Message {


    public static final String MINE = "bianco";
    public static final String OTHERS = "verde";
    private String messaggio;
    private String mittente;
    private String destinatario;
    private String tipo;

    private Message(){}

    public String getMessaggio() {

        return messaggio;
    }

    public String getMittente() {

        return mittente;
    }

    public String getDestinatario() {

        return destinatario;
    }

    public String getTipo() {

        return tipo;
    }

    public static class Builder {

        private String messaggio;
        private String mittente;
        private String destinatario;
        private String tipo;

        public Builder(){}

        public Builder setMittente(String mittente) {
            this.mittente = mittente;
            return this;
        }

        public Builder setMessaggio(String messaggio) {
            this.messaggio = messaggio;
            return this;
        }

        public Builder setDestinatario(String destinatario) {
            this.destinatario = destinatario;
            return this;
        }

        public Builder setTipo(String tipo) {
            this.tipo = tipo;
            return this;
        }

        public Message build(){

            Message message = new Message();
            message.messaggio = messaggio;
            message.mittente = mittente;
            message.destinatario = destinatario;
            message.tipo = tipo;

            return message;
        }
    }


}
