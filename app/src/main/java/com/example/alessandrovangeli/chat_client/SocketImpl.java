package com.example.alessandrovangeli.chat_client;

import android.app.Application;
import android.content.Context;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;

/**
 * Created by alessandro.vangeli on 24/11/2017.
 */

public class SocketImpl extends Application{

    private ActivityLifecycleCallbacks myLifecycleHandler;
    private Socket socket;
    {
        try {
            socket = IO.socket("http://172.31.98.217:3000");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onCreate() {
        super.onCreate();

        myLifecycleHandler=new MyLifecycleHandler();
        registerActivityLifecycleCallbacks(myLifecycleHandler);
    }

    public Socket getSocket() {

        return socket;
    }
}
