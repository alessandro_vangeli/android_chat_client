package com.example.alessandrovangeli.chat_client;

/**
 * Created by alessandro.vangeli on 12/12/2017.
 */

public class Utente {

    private String username, password;

    public Utente(String username, String password){

        this.password = password; this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public String getUtente() { return username; }
}
