package com.example.alessandrovangeli.chat_client;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.alessandrovangeli.chat_client.RetrofitService.ApiUtils;
import com.example.alessandrovangeli.chat_client.RetrofitService.RetrofitServiceInterface;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private Button login;
    private EditText username_text;
    private EditText password_text;
    private String username;
    private String password;
    private Socket socket;
    private Button registration;
    private RetrofitServiceInterface mService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SocketImpl appl = (SocketImpl)getApplication();
        socket = appl.getSocket();
        socket.connect();

        if(SharedPrefs.contains(getApplicationContext(), "log_state") && SharedPrefs.getLogState(getApplicationContext()).equals("logged_in")) {


            String user = SharedPrefs.getUser(getApplicationContext());
            Intent in = new Intent(LoginActivity.this, ChatListActivity.class);
            Bundle extras = new Bundle();
            extras.putString("mittente", user);
            in.putExtras(extras);
            startActivity(in);
            finish();
        }

        setContentView(R.layout.activity_login);
        username_text = (EditText) findViewById(R.id.username_input);
        password_text = (EditText) findViewById(R.id.password_input);

        login = (Button) findViewById(R.id.login_button);
        registration = (Button) findViewById(R.id.registration_button);

        socket.on("login_success", onLoginSuccess);
        socket.on("status", OnStatus);


    }


    @Override
    protected void onStart() {
        super.onStart();

        registration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class );
                startActivity(intent);
                finish();
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doLogin();

            }
        });
    }


    private void doLogin() {

        username = username_text.getText().toString().trim();
        password = password_text.getText().toString().trim();
        Utente utente = new Utente(username, password);
        String json_utente = new Gson().toJson(utente);

        if(username.equals("") || password.equals(""))
            Toast.makeText(LoginActivity.this, "Inserire la username e la password" +
                    "!", Toast.LENGTH_LONG).show();
        else
            socket.emit("login_control", json_utente);

    }


    private Emitter.Listener OnStatus = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String status = null;

                    try {
                        status = data.getString("message");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if(status.equals("incorrect user")){
                        Toast.makeText(LoginActivity.this, "username e/o password scorretti" +
                                "!", Toast.LENGTH_LONG).show();
                    }
                }
            });

        }
    };

    //login approvata
    private Emitter.Listener onLoginSuccess = new Emitter.Listener(){

        @Override
        public void call(Object... args) {

            JSONObject data = (JSONObject) args[0];
            String username = null;

            try {
                username = data.getString("username");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            SharedPrefs.saveUser(getApplicationContext(), username);

            //doFirebaseRegistration();


            Intent in = new Intent(LoginActivity.this, ChatListActivity.class );

            Bundle extras = new Bundle();
            extras.putString("mittente", username);
            in.putExtras(extras);
            startActivity(in);
            finish();

        }
    };


    private void doFirebaseRegistration() {

        String token = SharedPrefs.getFirebaseToken(getApplicationContext());

        HashMap<String,String> map = new HashMap<String,String>();
        map.put("username", username);
        map.put("token", token);

        mService = ApiUtils.getUserMainervice();

        mService.registerUser(map).enqueue(new Callback<String>() {

            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                String risposta = null;

                if (response.isSuccessful())
                    risposta = response.body();
                    Log.d("risposta", risposta);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

}
