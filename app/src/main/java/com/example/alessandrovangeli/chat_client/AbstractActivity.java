package com.example.alessandrovangeli.chat_client;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import io.socket.client.Socket;

/**
 * Created by alessandro.vangeli on 01/02/2018.
 */

public abstract class AbstractActivity extends AppCompatActivity {

    protected Socket socket;
    protected String user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        user = SharedPrefs.getUser(getApplicationContext());
        SocketImpl appl = (SocketImpl)getApplication();
        socket = appl.getSocket();
    }


    @Override
    protected void onResume() {
        super.onResume();

        if(MyLifecycleHandler.isComingFromBackGround())
            socket.emit("reconnection", user);
    }

    @Override
    protected void onStop() {
        super.onStop();

        if(!(MyLifecycleHandler.isApplicationInForeground()))
            socket.emit("status_offline", user);
    }

}
