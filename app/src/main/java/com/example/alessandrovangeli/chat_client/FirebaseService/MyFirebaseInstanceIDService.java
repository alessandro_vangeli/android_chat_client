package com.example.alessandrovangeli.chat_client.FirebaseService;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.session.MediaController;
import android.media.session.MediaSession;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.alessandrovangeli.chat_client.ChatListActivity;
import com.example.alessandrovangeli.chat_client.RetrofitService.ApiUtils;
import com.example.alessandrovangeli.chat_client.RetrofitService.RetrofitServiceInterface;
import com.example.alessandrovangeli.chat_client.SharedPrefs;
import com.example.alessandrovangeli.chat_client.SocketImpl;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by alessandro.vangeli on 18/12/2017.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private final String TAG = "FirebaseInstanceService";
    private RetrofitServiceInterface mService;
    private Context context;

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        context = getApplicationContext();

        sendRegistrationToServer(refreshedToken);

    }

    private void sendRegistrationToServer(String refreshedToken) {


        SharedPrefs.saveFirebaseToken(context, refreshedToken);


    }
    
    
}
